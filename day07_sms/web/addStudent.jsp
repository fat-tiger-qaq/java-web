<%--
  Created by IntelliJ IDEA.
  User: PANGHU
  Date: 2020/12/25
  Time: 19:58
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- 上述3个meta标签*必须*放在最前面，任何其他内容都*必须*跟随其后！ -->
    <title>添加学生</title>
    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <script src="../js/jquery-3.2.1.min.js"></script>
    <script src="../js/bootstrap.js"></script>

</head>
<body>
<form class="form-horizontal" action="${pageContext.request.contextPath}/addStudentServlet" method="post" >
    <div class="form-group">
        <label for="username" class="col-sm-2 control-label">姓名</label>
        <div class="col-sm-2">
            <input type="username" class="form-control" id="username" name="name" placeholder="请输入姓名">
        </div>
    </div>
    <div class="form-group">
        <label for="age" class="col-sm-2 control-label">年龄</label>
        <div class="col-sm-2">
            <input type="number" class="form-control" id="age" name="age" placeholder="请输入年龄">
        </div>
    </div>
    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <button type="submit" class="btn btn-default">提交</button>
        </div>
    </div>
</form>


</body>
</html>