package com.itheima.servlet;

import com.itheima.bean.Student;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

@WebServlet("/listStudentServlet")
public class ListStudentServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //读取文件并封装成学生对象
        BufferedReader br = new BufferedReader(new FileReader("D:\\IdeaProjects\\JavaWeb\\day07_sms\\src\\stu.txt"));


        ArrayList<Student> list = new ArrayList<>();
        String line;
        while ((line = br.readLine()) != null) {
            Student stu = new Student();
            String[] s = line.split(",");
            stu.setName(s[0]);
            stu.setAge(Integer.parseInt(s[1]));
            list.add(stu);

        }
        request.getSession().setAttribute("list",list);
        br.close();
        for (Student s : list) {
            System.out.println(s.getName());
        }

        //跳转到listStudent.jsp页面
        response.sendRedirect(request.getContextPath() + "/listStudent.jsp");
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}
