package com.itheima.filter;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

@WebFilter("/*")
public class EncodingFilter implements Filter {


    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws ServletException, IOException {

        try {
            //根据需要决定是否强转为HttpServletRequest和HttpServletResponse
            req.setCharacterEncoding("UTF-8");
            resp.setContentType("text/html;charset=UTF-8");

            chain.doFilter(req,resp);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }



    }


}
