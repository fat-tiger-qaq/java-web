package com.itheima.filter;

import com.sun.net.httpserver.HttpsServer;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebFilter(value = {"/addStudent.jsp","/listStudentServlet"})
public class LoginFilter implements Filter {
    public void destroy() {
    }

    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws ServletException, IOException {
        //强转为HttpServerRequest
        HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) resp;

        //获取属性
        String username = (String) request.getSession().getAttribute("username");
        System.out.println(username);

        if(username == null){
            response.sendRedirect(request.getContextPath() + "/login.jsp");
        }

        chain.doFilter(request,response);

    }

    public void init(FilterConfig config) throws ServletException {

    }

}
