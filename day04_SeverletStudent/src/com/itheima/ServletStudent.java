package com.itheima;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet("/studentServlet")
public class ServletStudent extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //获取表单数据
        String username = request.getParameter("username");
        String age = request.getParameter("age");
        String score = request.getParameter("score");
        //将数据写入文件中
        BufferedWriter bw = new BufferedWriter(new FileWriter("D:\\IdeaProjects\\JavaWeb\\day04_SeverletStudent\\web\\stu.txt",true));
        bw.write(username + "," + age + "," + score);
        bw.newLine();
        bw.close();

        //服务器给出反馈
        PrintWriter writer = response.getWriter();
        writer.println("Save saccess!");


    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}
