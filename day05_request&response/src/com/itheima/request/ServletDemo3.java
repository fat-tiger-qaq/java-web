package com.itheima.request;

import sun.misc.Request;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;
import java.util.Map;

@WebServlet("/ServletDemo3")
public class ServletDemo3 extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
       //表单值
        String username = request.getParameter("username");
        System.out.println(username);

        System.out.println("--------------------");
        //表单复选框的值
        String[] hobbys = request.getParameterValues("hobby");
        for(String hobby : hobbys){
            System.out.println(hobby);
        }

        System.out.println("--------------------");
        //用map集合获取所有的键值
        Map<String, String[]> map = request.getParameterMap();
        map.forEach((k,v) ->{
            System.out.println(k + "==" + Arrays.toString(map.get(k)));
        });
    }

    //所有的键值对


    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}
