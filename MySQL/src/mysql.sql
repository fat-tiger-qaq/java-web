-- 创建数据库
CREATE DATABASE db1;

-- 创建数据库,判断不存在,在创建
CREATE DATABASE IF NOT EXISTS db2;

-- 创建数据库,并指定字符集
CREATE DATABASE db3 CHARACTER SET utf8;

-- 创建db4数据库,判断是否村子,并制定字符集gbk
CREATE DATABASE IF NOT EXISTS db4 CHARACTER SET gbk;

-- 查询所有数据库名称
SHOW DATABASES;

-- 使用数据库
USE db1;

-- 查询当前正在使用的库
SELECT DATABASE();

-- 删除数据库名称
DROP DATABASE db1;

-- 如果数据库存在就删除数据库名称
DROP DATABASE IF EXISTS db1;

-- 修改数据库字符集
alter database db1 character set gbk;

-- 创建一个product商品表(商品编号 商品名称 商品价格 商品库存 上架时间)
user db1;
create table product(
	id int,
	name varchar(20),
	price double,
	stock int,
	insert_time date
);

desc  product;

-- 向product表中添加一条数据 给指定列
insert into product(id,name,price,stock,insert_time) values (1,'手机',18.888,60,'2020-12-27');

-- 删除所有数据 逐一删除,效率低
delete from product;

-- 删除所有数据 删除原来的表重新创建一个
truncate product;

use db1;
create table a(id int);


-- 创建数据表
CREATE TABLE product2(
	id INT,			-- 商品编号
	NAME VARCHAR(20),	-- 商品名称
	price DOUBLE,		-- 商品价格
	brand VARCHAR(10),	-- 商品品牌
	stock INT,		-- 商品库存
	insert_time DATE        -- 添加时间
);

-- 添加数据
INSERT INTO product2 VALUES 
(1,'华为手机',3999,'华为',23,'2088-03-10'),
(2,'小米手机',2999,'小米',30,'2088-05-15'),
(3,'苹果手机',5999,'苹果',18,'2088-08-20'),
(4,'华为电脑',6999,'华为',14,'2088-06-16'),
(5,'小米电脑',4999,'小米',26,'2088-07-08'),
(6,'苹果电脑',8999,'苹果',15,'2088-10-25'),
(7,'联想电脑',7999,'联想',NULL,'2088-11-11');

drop table a;


select * from product2;

select * from product2 WHERE NAME='华为手机' ;

select id,name,stock from  product2 order by id desc;

select * from product2 where stock in(23,14);

insert into product2 values(8,'索尼大法好',1999,'索尼',66,'2020/11/20');

select name,price as money from product2   where price > 2999; 

select * from product2 where name like '%米%';

select * from product2 where stock is not null,`product``product2``product`;

select * from product2 where price between 3000 and 6000;

select name,sum(price) getSum from product2 where name like'%电脑%' GROUP by brand having getSum > 5000 ORDER BY getSum DESC;



SELECT brand,SUM(price) getSum FROM product WHERE price > 4000 GROUP BY brand HAVING getSum > 7000 ORDER BY getSum DESC;

create table stu(id int primary key, name varchar(20));

insert into stu values(1,'张三'),(2,'李四');

insert into stu values(3,'王五');

-- 添加自动增长
alter table stu modify id int auto_increment;

insert into stu values (null,'胖虎');

-- 删除自动增长
alter table stu modify id int;

-- 添加唯一约束
alter table stu modify id int unique;

insert into stu values(7,'张三');


INSERT INTO stu VALUES(NULL,'路人甲');

desc stu;

create table users(id int primary key,name varchar(20) not null);

create table orderlist(id int primary key AUTO_INCREMENT,number varchar(20) not null,uid int,constraint ou_fk1 foreign key(uid) references users(id));

alter table users modify name varchar(20) not null;

desc orderlist;

desc users;

alter table users modify id int not null;



-- 使用db3数据库
USE db3;

-- 创建person表
CREATE TABLE person(
	id INT PRIMARY KEY AUTO_INCREMENT,	-- 主键id
	NAME VARCHAR(20)                        -- 姓名
);
-- 添加数据
INSERT INTO person VALUES (NULL,'张三'),(NULL,'李四');

-- 创建card表
CREATE TABLE card(
	id INT PRIMARY KEY AUTO_INCREMENT,	-- 主键id
	number VARCHAR(20) UNIQUE NOT NULL,	-- 身份证号
	pid INT UNIQUE,                         -- 外键列
	CONSTRAINT cp_fk1 FOREIGN KEY (pid) REFERENCES person(id)
);
-- 添加数据
INSERT INTO card VALUES (NULL,'12345',1),(NULL,'56789',2);






-- 使用db4数据库
USE db4;

-- 创建user表
CREATE TABLE USER(
	id INT PRIMARY KEY AUTO_INCREMENT,	-- 用户id
	NAME VARCHAR(20),			-- 用户姓名
	age INT                                 -- 用户年龄
);
-- 添加数据
INSERT INTO USER VALUES (1,'张三',23);
INSERT INTO USER VALUES (2,'李四',24);
INSERT INTO USER VALUES (3,'王五',25);
INSERT INTO USER VALUES (4,'赵六',26);


-- 订单表
CREATE TABLE orderlist(
	id INT PRIMARY KEY AUTO_INCREMENT,	-- 订单id
	number VARCHAR(30),			-- 订单编号
	uid INT,    -- 外键字段
	CONSTRAINT ou_fk1 FOREIGN KEY (uid) REFERENCES USER(id)
);
-- 添加数据
INSERT INTO orderlist VALUES (1,'hm001',1);
INSERT INTO orderlist VALUES (2,'hm002',1);
INSERT INTO orderlist VALUES (3,'hm003',2);
INSERT INTO orderlist VALUES (4,'hm004',2);
INSERT INTO orderlist VALUES (5,'hm005',3);
INSERT INTO orderlist VALUES (6,'hm006',3);
INSERT INTO orderlist VALUES (7,'hm007',NULL);


-- 商品分类表
CREATE TABLE category(
	id INT PRIMARY KEY AUTO_INCREMENT,  -- 商品分类id
	NAME VARCHAR(10)                    -- 商品分类名称
);
-- 添加数据
INSERT INTO category VALUES (1,'手机数码');
INSERT INTO category VALUES (2,'电脑办公');
INSERT INTO category VALUES (3,'烟酒茶糖');
INSERT INTO category VALUES (4,'鞋靴箱包');


-- 商品表
CREATE TABLE product(
	id INT PRIMARY KEY AUTO_INCREMENT,   -- 商品id
	NAME VARCHAR(30),                    -- 商品名称
	cid INT, -- 外键字段
	CONSTRAINT cp_fk1 FOREIGN KEY (cid) REFERENCES category(id)
);
-- 添加数据
INSERT INTO product VALUES (1,'华为手机',1);
INSERT INTO product VALUES (2,'小米手机',1);
INSERT INTO product VALUES (3,'联想电脑',2);
INSERT INTO product VALUES (4,'苹果电脑',2);
INSERT INTO product VALUES (5,'中华香烟',3);
INSERT INTO product VALUES (6,'玉溪香烟',3);
INSERT INTO product VALUES (7,'计生用品',NULL);


-- 中间表
CREATE TABLE us_pro(
	upid INT PRIMARY KEY AUTO_INCREMENT,  -- 中间表id
	uid INT, -- 外键字段。需要和用户表的主键产生关联
	pid INT, -- 外键字段。需要和商品表的主键产生关联
	CONSTRAINT up_fk1 FOREIGN KEY (uid) REFERENCES USER(id),
	CONSTRAINT up_fk2 FOREIGN KEY (pid) REFERENCES product(id)
);
-- 添加数据
INSERT INTO us_pro VALUES (NULL,1,1);
INSERT INTO us_pro VALUES (NULL,1,2);
INSERT INTO us_pro VALUES (NULL,1,3);
INSERT INTO us_pro VALUES (NULL,1,4);
INSERT INTO us_pro VALUES (NULL,1,5);
INSERT INTO us_pro VALUES (NULL,1,6);
INSERT INTO us_pro VALUES (NULL,1,7);
INSERT INTO us_pro VALUES (NULL,2,1);
INSERT INTO us_pro VALUES (NULL,2,2);
INSERT INTO us_pro VALUES (NULL,2,3);
INSERT INTO us_pro VALUES (NULL,2,4);
INSERT INTO us_pro VALUES (NULL,2,5);
INSERT INTO us_pro VALUES (NULL,2,6);
INSERT INTO us_pro VALUES (NULL,2,7);
INSERT INTO us_pro VALUES (NULL,3,1);
INSERT INTO us_pro VALUES (NULL,3,2);
INSERT INTO us_pro VALUES (NULL,3,3);
INSERT INTO us_pro VALUES (NULL,3,4);
INSERT INTO us_pro VALUES (NULL,3,5);
INSERT INTO us_pro VALUES (NULL,3,6);
INSERT INTO us_pro VALUES (NULL,3,7);
INSERT INTO us_pro VALUES (NULL,4,1);
INSERT INTO us_pro VALUES (NULL,4,2);
INSERT INTO us_pro VALUES (NULL,4,3);
INSERT INTO us_pro VALUES (NULL,4,4);
INSERT INTO us_pro VALUES (NULL,4,5);
INSERT INTO us_pro VALUES (NULL,4,6);
INSERT INTO us_pro VALUES (NULL,4,7);

-- 显示内链接
select
	*					-- 查询内容
from 
	user u inner join orderlist o 		-- 查询的数据源
on
	u.`id`=o.`uid`;				-- 查询条件
	
	
	
	
-- 隐示内链接
SELECT
	u.name,u.age,o.number		-- 查询内容
FROM 
	USER u , orderlist o 		-- 查询的数据源
where
	u.`id`=o.`uid`;			-- 查询条件
	
	
-- 查询年龄最高的用户名
select
	name,age
from
	user
where
	age=(select max(age) from user);
	
	
-- 查询张三和李四的订单信息
select * from orderlist where uid in(select id from user where name in('张三','李四'));


-- 查询订单表中id大于4的订单信息和所属用户
select * from orderlist o ,user u where o.uid = u.id and o.id>4;

