package com.itheima;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;

@WebServlet("/ListStudent")
public class ListStudent extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //读取文件的数据
        BufferedReader br = new BufferedReader(new FileReader("D:\\IdeaProjects\\JavaWeb\\day05_Student\\web\\stu.txt"));
        //更改字符编码
        response.setContentType("text/html;charset=UTF-8");
        //导入jar包获得输出流对象
        PrintWriter pw = response.getWriter();
        //将文件写到浏览器中
        String line;
        while ((line = br.readLine()) != null){
            pw.write(line + "<br>");
        }
        //关流
        br.close();

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}
